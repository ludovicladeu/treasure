# CarbonIt Treasure exercise

### build project
```bash
mvn clean package
```

### run test
```bash
mvn test
```

### run project thought terminal
```bash
# filename should be in resources folder ex: map.txt
mvn exec:java -Dexec.mainClass="com.carbonit.exercise.BootStrap" -Dexec.args="map.txt"
```
**`NB:`** result is at the root directory in __map_output.txt__