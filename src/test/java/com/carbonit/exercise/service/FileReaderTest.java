package com.carbonit.exercise.service;

import com.carbonit.exercise.model.Map;
import com.carbonit.exercise.model.Mountain;
import com.carbonit.exercise.model.Treasure;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FileReaderTest {
    private Reader reader = new FileReader();

    @Test
    void should_parse_file() {
        // Given

        String filename = "map.txt";

        // When
        Map map = reader.parse(filename);

        // Then
        Mountain mountain1 = new Mountain(1, 0);
        Mountain mountain2 = new Mountain(2, 1);
        Treasure treasure1 = new Treasure(0, 3, 2);
        Treasure treasure2 = new Treasure(1, 3, 3);
        assertThat(map.getWidth()).isEqualTo(3);
        assertThat(map.getHeight()).isEqualTo(4);
        assertThat(map.getMountains().size()).isEqualTo(2);
        assertThat(map.getMountains().get(mountain1.getPosition())).isEqualTo(mountain1);
        assertThat(map.getMountains().get(mountain2.getPosition())).isEqualTo(mountain2);

        assertThat(map.getTreasures().size()).isEqualTo(2);
        assertThat(map.getTreasures().get(treasure1.getPosition())).isEqualTo(treasure1);
        assertThat(map.getTreasures().get(treasure2.getPosition())).isEqualTo(treasure2);
    }
}
