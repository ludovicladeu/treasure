package com.carbonit.exercise.service;

import com.carbonit.exercise.model.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class TreasureServiceTest {

    private TreasureService treasureService = new TreasureService();

    @Test
    void should_find_treasure_in_map() {
        // Given
        Map map = new Map(3, 4);

        map.addMountain(new Mountain(1, 0));
        map.addMountain(new Mountain(2, 1));

        Treasure treasure1 = new Treasure(0, 3, 2);
        map.addTreasure(treasure1);

        Treasure treasure2 = new Treasure(1, 3, 3);
        map.addTreasure(treasure2);

        Adventurer adventurer = new Adventurer("Lara", 1, 1, Orientation.SOUTH);
        List<Movement> movements = List.of(Movement.A, Movement.A,
                Movement.D, Movement.A,
                Movement.D, Movement.A,
                Movement.G, Movement.G,
                Movement.A);
        map.addAdventurers(adventurer, movements);

        // When
        treasureService.findTreasure(map);

        // Then
        assertThat(treasure1.getCount()).isEqualTo(0);
        assertThat(treasure2.getCount()).isEqualTo(2);

        assertThat(adventurer.getPosition()).isEqualTo(new Position(0, 3));
        assertThat(adventurer.getName()).isEqualTo("Lara");
        assertThat(adventurer.getTreasure()).isEqualTo(3);
        assertThat(adventurer.getOrientation()).isEqualTo(Orientation.SOUTH);
    }
}