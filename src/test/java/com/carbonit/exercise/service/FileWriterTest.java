package com.carbonit.exercise.service;

import com.carbonit.exercise.model.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

class FileWriterTest {

    private Writer fileWriter = new FileWriter();

    private Path resultFilePath;

    @AfterEach
    void afterTest() throws IOException {
        Files.delete(resultFilePath);
    }

    @Test
    void should_write_map_in_file() throws IOException {
        // Given
        Map map = new Map(4, 4);

        map.addMountain(new Mountain(1, 2 ));

        Treasure treasure = new Treasure(1, 1, 2);
        map.addTreasure(treasure);
        map.addTreasure(new Treasure(2, 0, 0));

        Adventurer jessy = new Adventurer("Jessy", 3, 2, Orientation.NORTH);
        jessy.addTreasure(treasure);
        map.addAdventurers(jessy, emptyList());
        map.addAdventurers(new Adventurer("Paul", 2, 2, Orientation.SOUTH), emptyList());
        map.addAdventurers(new Adventurer("Paul2", 2, 2, Orientation.NORTH), emptyList());

        // When
        resultFilePath = fileWriter.write(map);

        // Then
        List<String> fileContent = Files.readAllLines(resultFilePath);

        assertThat(fileContent.size()).isEqualTo(7);
        assertThat(fileContent.get(0)).isEqualTo("C - 4 - 4");
        assertThat(fileContent.get(1)).isEqualTo("M - 1 - 2");
        assertThat(fileContent.get(2)).isEqualTo("# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors restants}");
        assertThat(fileContent.get(3)).isEqualTo("T - 1 - 1 - 1");
        assertThat(fileContent.get(4)).isEqualTo("# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Nb. trésors ramassés}");
        assertThat(fileContent.get(5)).isEqualTo("A - Jessy - 3 - 2 - N - 1");
        assertThat(fileContent.get(6)).isEqualTo("A - Paul - 2 - 2 - S - 0");
    }

    @Test
    void should_write_map_when_no_treasure_remain() throws IOException {
        // Given
        Map map = new Map(4, 4);

        map.addMountain(new Mountain(1, 2 ));

        map.addTreasure(new Treasure(1, 1, 0));
        map.addTreasure(new Treasure(2, 0, 0));

        map.addAdventurers(new Adventurer("Paul", 2, 2, Orientation.WEST), emptyList());

        // When
        resultFilePath = fileWriter.write(map);

        // Then
        List<String> fileContent = Files.readAllLines(resultFilePath);

        System.out.println(fileContent);

        assertThat(fileContent.size()).isEqualTo(4);
        assertThat(fileContent.get(0)).isEqualTo("C - 4 - 4");
        assertThat(fileContent.get(1)).isEqualTo("M - 1 - 2");
        assertThat(fileContent.get(2)).isEqualTo("# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Nb. trésors ramassés}");
        assertThat(fileContent.get(3)).isEqualTo("A - Paul - 2 - 2 - O - 0");

    }
}