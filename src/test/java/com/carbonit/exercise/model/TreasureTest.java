package com.carbonit.exercise.model;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TreasureTest {


    @Test
    void should_remove_part_of_treasure() {
        // Given
        Treasure treasure = new Treasure(3, 4, 10);

        // When
        treasure.removePart();

        // Then
        assertThat(treasure.getCount()).isEqualTo(9);
    }

    @Test
    void should_not_remove_part_of_treasure_when_nothing_rest() {
        // Given
        Treasure treasure = new Treasure(3, 4, 0);

        // When
        treasure.removePart();

        // Then
        assertThat(treasure.getCount()).isEqualTo(0);
    }
}
