package com.carbonit.exercise.model;

import org.junit.jupiter.api.Test;

import static com.carbonit.exercise.model.Orientation.SOUTH;
import static org.assertj.core.api.Assertions.assertThat;

class AdventurerTest {

    @Test
    void should_move_forward_when_no_obstacle_and_movement_A() {
        // Given
        Map map = new Map(3, 3);
        map.addMountain(new Mountain(2, 2));

        Treasure treasure = new Treasure(2, 1, 0);
        map.addTreasure(treasure);
        Adventurer adventurer = new Adventurer("Tom", 2, 0, SOUTH);
        adventurer.setMap(map);

        // When
        adventurer.move(Movement.A);


        // Then
        assertThat(adventurer.getPosition()).isEqualTo(new Position(2, 1));
        assertThat(adventurer.getTreasure()).isEqualTo(0);
        assertThat(treasure.getCount()).isEqualTo(0);
    }

    @Test
    void should_move_forward_and_get_treasure_when_no_obstacle_and_movement_A() {
        // Given
        Map map = new Map(3, 3);
        map.addMountain(new Mountain(2, 2));
        Treasure treasure = new Treasure(2, 1, 2);
        map.addTreasure(treasure);
        Adventurer adventurer = new Adventurer("Tom", 2, 0, SOUTH);
        adventurer.setMap(map);

        // When
        adventurer.move(Movement.A);

        // Then
        assertThat(adventurer.getPosition()).isEqualTo(new Position(2, 1));
        assertThat(adventurer.getTreasure()).isEqualTo(1);
        assertThat(treasure.getCount()).isEqualTo(1);
    }

    @Test
    void should_not_move_forward_when_there_is_obstacle_and_movement_A() {
        // Given
        Map map = new Map(3, 3);
        map.addMountain(new Mountain(2, 2));

        Adventurer adventurer = new Adventurer("Tom", 2, 1, SOUTH);
        adventurer.setMap(map);

        // When
        adventurer.move(Movement.A);

        // Then
        assertThat(adventurer.getPosition()).isEqualTo(new Position(2, 1));
        assertThat(adventurer.getTreasure()).isEqualTo(0);
    }

    @Test
    void should_turn_left_when_movement_is_G() {
        // Given
        Map map = new Map(3, 3);
        map.addMountain(new Mountain(2, 2));

        Adventurer adventurer = new Adventurer("Tom", 2, 1, SOUTH);
        adventurer.setMap(map);

        // When
        adventurer.move(Movement.G);

        // Then
        assertThat(adventurer.getOrientation()).isEqualTo(Orientation.EAST);
        assertThat(adventurer.getTreasure()).isEqualTo(0);
    }

    @Test
    void should_turn_right_when_movement_is_D() {
        // Given
        Map map = new Map(3, 3);
        map.addMountain(new Mountain(2, 2));
        Treasure treasure = new Treasure(2, 1, 2);
        map.addTreasure(treasure);

        Adventurer adventurer = new Adventurer("Tom", 2, 1, SOUTH);
        adventurer.setMap(map);

        // When
        adventurer.move(Movement.D);

        // Then
        assertThat(adventurer.getOrientation()).isEqualTo(Orientation.WEST);
        assertThat(adventurer.getTreasure()).isEqualTo(0);
        assertThat(treasure.getCount()).isEqualTo(2);
    }

}