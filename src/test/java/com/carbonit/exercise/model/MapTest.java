package com.carbonit.exercise.model;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static com.carbonit.exercise.model.Orientation.NORTH;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

public class MapTest {

    private static Stream<Arguments> createMapParameters() {
        Map map = new Map(4, 5);
        return Stream.of(
                Arguments.of(map, new Position(3, 4), true),
                Arguments.of(map, new Position(0, 4), true),
                Arguments.of(map, new Position(3, 0), true),

                Arguments.of(map, new Position(4, 4), false),
                Arguments.of(map, new Position(-1, 4), false),
                Arguments.of(map, new Position(0, 5), false),
                Arguments.of(map, new Position(0, -1), false)
        );
    }

    @ParameterizedTest
    @MethodSource("createMapParameters")
    void should_check_if_position_is_in_area(Map map, Position position, boolean expectedIsPositionInArea) {

        // When
        boolean isPositionInArea = map.isPositionInArea(position);

        // Then
        assertThat(isPositionInArea).isEqualTo(expectedIsPositionInArea);
    }

    private static Stream<Arguments> createObstacleMapParameters() {
        Map map = new Map(3, 4);
        map.addMountain(new Mountain(1, 1 ));
        map.addAdventurers(new Adventurer("test", 1, 2, NORTH), emptyList());
        return Stream.of(
                Arguments.of(map, new Position(3, 4), false),
                Arguments.of(map, new Position(1, 2), true),
                Arguments.of(map, new Position(1, 1), true)
        );
    }

    @ParameterizedTest
    @MethodSource("createObstacleMapParameters")
    void should_check_if_is_an_obstacle_position(Map map, Position position, boolean expectedIsObstaclePosition) {

        // When
        boolean isObstacle = map.isObstacle(position);

        // Then
        assertThat(isObstacle).isEqualTo(expectedIsObstaclePosition);
    }
}
