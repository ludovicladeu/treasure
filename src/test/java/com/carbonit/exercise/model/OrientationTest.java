package com.carbonit.exercise.model;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class OrientationTest {

    private static Stream<Arguments> createTurnParameters() {
        return Stream.of(
                Arguments.of(Orientation.NORTH, Movement.D, Orientation.EAST),
                Arguments.of(Orientation.NORTH, Movement.G, Orientation.WEST),

                Arguments.of(Orientation.SOUTH, Movement.D, Orientation.WEST),
                Arguments.of(Orientation.SOUTH, Movement.G, Orientation.EAST),

                Arguments.of(Orientation.EAST, Movement.D, Orientation.SOUTH),
                Arguments.of(Orientation.EAST, Movement.G, Orientation.NORTH),

                Arguments.of(Orientation.WEST, Movement.D, Orientation.NORTH),
                Arguments.of(Orientation.WEST, Movement.G, Orientation.SOUTH)
        );
    }

    @ParameterizedTest(name = "When orientation {0} and turn {1} then next orientation should be {2}")
    @MethodSource("createTurnParameters")
    void should_get_the_next_orientation(Orientation orientation, Movement movement, Orientation expectedOrientation) {
        // When
        Orientation nextOrientation = orientation.turn(movement);

        // Then
        assertThat(nextOrientation).isEqualTo(expectedOrientation);
    }


    private static Stream<Arguments> createMoveForwardParameters() {
        Position position = new Position(1, 1);
        return Stream.of(
                Arguments.of(position, Orientation.NORTH, new Position(1, 0)),
                Arguments.of(position, Orientation.SOUTH, new Position(1, 2)),
                Arguments.of(position, Orientation.EAST, new Position(2, 1)),
                Arguments.of(position, Orientation.WEST, new Position(0, 1))
        );
    }

    @ParameterizedTest(name = "When current position is {0} and orientation {1} move forward then next position should be {2}")
    @MethodSource("createMoveForwardParameters")
    void should_get_the_next_position(Position position, Orientation orientation, Position expectedPosition) {
        // When
        Position nextPosition = orientation.moveForward(position);

        // Then
        assertThat(nextPosition).isEqualTo(expectedPosition);
    }

}
