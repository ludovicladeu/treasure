package com.carbonit.exercise.exception;

public class WriterError extends RuntimeException{
    public WriterError(String message, Throwable cause) {
        super(message, cause);
    }
}
