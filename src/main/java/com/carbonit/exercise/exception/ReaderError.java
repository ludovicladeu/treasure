package com.carbonit.exercise.exception;

public class ReaderError extends RuntimeException {
    public ReaderError(String message, Throwable cause) {
        super(message, cause);
    }
}
