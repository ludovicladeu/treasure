package com.carbonit.exercise.service;

import com.carbonit.exercise.model.Map;

import java.nio.file.Path;

public interface Writer {
    Path write(Map map);
}
