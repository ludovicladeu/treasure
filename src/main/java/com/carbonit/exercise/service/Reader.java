package com.carbonit.exercise.service;

import com.carbonit.exercise.model.Map;

public interface Reader {
    Map parse(String input);
}
