package com.carbonit.exercise.service;

import com.carbonit.exercise.exception.ReaderError;
import com.carbonit.exercise.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.toList;

public class FileReader implements Reader {

    private static final Logger LOG = LoggerFactory.getLogger(FileReader.class);

    private List<String[]> readAndCleanFile(String filename) {
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            var file = classloader.getResource(filename);
            Path filePath = Paths.get(file.getPath());
            System.out.println(filePath);
            return Files.readAllLines(filePath).stream()
                    .filter(not(String::isBlank))
                    .filter(line -> !line.startsWith("#"))
                    .map(line -> line.replace(" ", ""))
                    .map(line -> line.split("-"))
                    .collect(toList());
        } catch (Exception e) {
            throw new ReaderError("Error when read file", e);
        }
    }

    private Map parse(List<String[]> fileContent) {
        var map = new Map();
        for (var line : fileContent) {
            try {
                parseRow(map, line);
            } catch (Exception e) {
                LOG.error(format("error unable to read line %s", line.toString()), e);
                throw new ReaderError("Error when read line", e);
            }
        }

        return map;
    }

    private void parseRow(Map map, String[] line) {
        switch (line[0]) {

            case "C":
                int width = getNumber(line[1]);
                int height = getNumber(line[2]);
                map.setWidth(width);
                map.setHeight(height);
                break;
            case "M":
                int xM = getNumber(line[1]);
                int yM = getNumber(line[2]);
                var mountain = new Mountain(xM, yM);
                map.addMountain(mountain);
                break;
            case "T":
                int xT = getNumber(line[1]);
                int yT = getNumber(line[2]);
                int countTreasure = getNumber(line[3]);
                var treasure = new Treasure(xT, yT, countTreasure);
                map.addTreasure(treasure);
                break;

            case "A":
                String name = line[1];
                int xA = getNumber(line[2]);
                int yA = getNumber(line[3]);
                Orientation.find(line[4])
                        .ifPresentOrElse(orientation -> {
                            var movements = line[5].chars()
                                    .mapToObj(c -> String.valueOf((char) c))
                                    .map(Movement::find)
                                    .filter(Optional::isPresent)
                                    .map(Optional::get)
                                    .collect(toList());
                            var adventurer = new Adventurer(name, xA, yA, orientation);
                            map.addAdventurers(adventurer, movements);
                        }, () -> LOG.warn("Adventurer {} has an unknown orientation", name));


                break;
            default:
                LOG.warn("The type {} is not recognized", line[0]);
        }
    }

    private int getNumber(String s) {
        return Integer.parseInt(s);
    }

    @Override
    public Map parse(String filename) {
        List<String[]> fileContent = readAndCleanFile(filename);
        return parse(fileContent);
    }
}
