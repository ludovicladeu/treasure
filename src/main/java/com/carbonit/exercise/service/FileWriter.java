package com.carbonit.exercise.service;

import com.carbonit.exercise.exception.WriterError;
import com.carbonit.exercise.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.joining;

public class FileWriter implements Writer {

    private final Logger LOGGER = LoggerFactory.getLogger(FileWriter.class);

    @Override
    public Path write(Map map) {
        try {
            String mountainsBody = mountainsBody(map.getMountains().values());
            String treasureBody = treasureBody(map.getTreasures().values());
            String adventurersBody = adventurersBody(map.getAdventurers().values());
            String fileContent =
                    map.toString()
                            + appendOnNewLine(mountainsBody)
                            + appendOnNewLine(treasureBody)
                            + appendOnNewLine(adventurersBody);

            return Files.writeString(Paths.get("map_output.txt"), fileContent, StandardOpenOption.CREATE);
        } catch (IOException e) {
            LOGGER.error("Error when write file", e);
            throw new WriterError("Error when write file", e);
        }
    }


    private String appendOnNewLine(String text) {
        if (text.isBlank()) {
            return "";
        }
        return "\n" + text;
    }


    private String mountainsBody(Collection<Mountain> mountains) {
        return mountains.stream()
                .map(Mountain::toString)
                .collect(joining("\n"));
    }

    private String treasureBody(Collection<Treasure> treasures) {
        String treasurePrint = treasures.stream()
                .filter(Treasure::isTreasureRemain)
                .map(Treasure::toString)
                .collect(joining("\n"));

        if (treasurePrint.isBlank()) {
            return "";
        }
        return "# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors restants}\n"
                + treasurePrint;
    }

    private String adventurersBody(Collection<SimpleEntry<Adventurer, List<Movement>>> adventurers) {
        return "# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Nb. trésors ramassés}\n"
                + adventurers.stream()
                .map(SimpleEntry::getKey)
                .map(Adventurer::toString)
                .collect(joining("\n"));
    }
}
