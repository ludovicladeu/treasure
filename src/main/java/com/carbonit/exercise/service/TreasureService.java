package com.carbonit.exercise.service;

import com.carbonit.exercise.model.Adventurer;
import com.carbonit.exercise.model.Map;
import com.carbonit.exercise.model.Movement;

import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import static java.util.Comparator.comparing;

public class TreasureService {

    public void findTreasure(Map map) {

        Collection<SimpleEntry<Adventurer, List<Movement>>> adventurersWithMovements = map.getAdventurers().values();
        var maxActionsLength = adventurersWithMovements.stream()
                .map(SimpleEntry::getValue)
                .map(List::size)
                .max(comparing(Integer::intValue))
                .get();

        for (int i = 0; i < maxActionsLength; i++) {
            moveEachAdventurerInTurn(adventurersWithMovements, i);
        }

    }

    private void moveEachAdventurerInTurn(Collection<SimpleEntry<Adventurer, List<Movement>>> adventurersWithMovements,
                                          int turnIndex) {
        adventurersWithMovements
                .stream()
                .filter(isAdventurerMovementsSizeHigherThanTurn(turnIndex))
                .forEach(entry -> {
                    Adventurer key = entry.getKey();
                    List<Movement> value = entry.getValue();
                    key.move(value.get(turnIndex));
                });
    }

    private Predicate<SimpleEntry<Adventurer, List<Movement>>> isAdventurerMovementsSizeHigherThanTurn(int turnIndex) {
        return entry -> entry.getValue().size() > turnIndex;
    }
}
