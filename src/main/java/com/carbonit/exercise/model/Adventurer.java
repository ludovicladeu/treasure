package com.carbonit.exercise.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class Adventurer {

    private final Logger LOGGER = LoggerFactory.getLogger(Adventurer.class);

    private final String name;
    private final Position position;
    private Orientation orientation;
    private int treasure;
    private Map map;

    public Adventurer(String name, int x, int y, Orientation orientation) {
        this.name = name;
        this.position = new Position(x, y);
        this.orientation = orientation;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public void addTreasure(Treasure treasure) {
        if (treasure.removePart()) {
            this.treasure++;
        }
    }

    public String getName() {
        return name;
    }

    public Position getPosition() {
        return position;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public int getTreasure() {
        return treasure;
    }

    public void move(Movement movement) {
        if (Movement.A.equals(movement)) {
            Position nextPosition = this.orientation.moveForward(this.position);
            this.actionForward(nextPosition);
        } else {
            this.orientation = this.orientation.turn(movement);
        }
    }


    private void actionForward(final Position position) {
        if (this.map != null && this.map.isPositionInArea(position) && !this.map.isObstacle(position)) {
            this.position.setX(position.getX());
            this.position.setY(position.getY());

            this.map.getTreasure(this.position)
                    .ifPresent(this::addTreasure);
        } else {
            LOGGER.warn("action=forward user={} can't move to position {}", name, position);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Adventurer that = (Adventurer) o;
        return Objects.equals(position, that.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position);
    }

    @Override
    public String toString() {
        return String.format("A - %s - %d - %d - %s - %d", name, position.getX(), position.getY(),
                orientation.getOrientation(), treasure);
    }

}
