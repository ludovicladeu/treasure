package com.carbonit.exercise.model;

import java.util.Optional;
import java.util.stream.Stream;

public enum Movement {
    A("A"),
    G("G"),
    D("D");

    private final String action;

    Movement(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public static Optional<Movement> find(String action) {
        return Stream.of(Movement.values())
                .filter(value -> value.action.equals(action))
                .findFirst();
    }
}
