package com.carbonit.exercise.model;

import java.util.Objects;

public class Mountain {

    private final Position position;

    public Mountain(int x, int y) {
        position = new Position(x, y);
    }

    public Position getPosition() {
        return position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mountain mountain = (Mountain) o;
        return position.equals(mountain.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position);
    }

    @Override
    public String toString() {
        return String.format("M - %d - %d", position.getX(), position.getY());
    }
}
