package com.carbonit.exercise.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

public class Map {
    private final Logger LOGGER = LoggerFactory.getLogger(Map.class);
    private int height;
    private int width;
    private final java.util.Map<Position, Mountain> mountains = new HashMap<>();
    private final java.util.Map<Position, Treasure> treasures = new HashMap<>();
    private java.util.Map<Position, SimpleEntry<Adventurer, List<Movement>>> adventurers = new LinkedHashMap<>();

    public Map() {
    }

    public Map(int width, int height) {
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }


    public int getWidth() {
        return width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public java.util.Map<Position, Mountain> getMountains() {
        return mountains;
    }

    public java.util.Map<Position, Treasure> getTreasures() {
        return treasures;
    }

    public void addMountain(Mountain mountain) {
        if (isPositionInArea(mountain.getPosition())) {
            this.mountains.put(mountain.getPosition(), mountain);
        } else {
            LOGGER.warn("action=add type=mountain  position {} is not in map area ", mountain.getPosition());
        }
    }

    public void addTreasure(Treasure treasure) {
        if (isPositionInArea(treasure.getPosition())) {
            this.treasures.put(treasure.getPosition(), treasure);
        } else {
            LOGGER.warn("action=add type=treasure  position {} is not in map area ", treasure.getPosition());
        }
    }

    public void addAdventurers(Adventurer adventurer, List<Movement> movements) {
        if (isPositionInArea(adventurer.getPosition()) && !this.adventurers.containsKey(adventurer.getPosition())) {
            this.adventurers.put(adventurer.getPosition(), new SimpleEntry<>(adventurer, movements));
            adventurer.setMap(this);
            this.getTreasure(adventurer.getPosition())
                    .ifPresent(adventurer::addTreasure);
        } else {
            LOGGER.warn("action=add type=adventurer  position {} is not in map area or key is already present", adventurer.getPosition());
        }
    }

    public java.util.Map<Position, SimpleEntry<Adventurer, List<Movement>>> getAdventurers() {
        return adventurers;
    }

    Optional<Treasure> getTreasure(Position position) {
        return Optional.ofNullable(treasures.get(position));
    }

    boolean isPositionInArea(final Position position) {
        return (0 <= position.getX() && position.getX() < this.width)
                && (0 <= position.getY() && position.getY() < this.height);
    }

    boolean isObstacle(Position position) {
        return this.adventurers.get(position) != null
                ||
                this.mountains.get(position) != null;
    }

    @Override
    public String toString() {
        return String.format("C - %d - %d", width, height);
    }
}
