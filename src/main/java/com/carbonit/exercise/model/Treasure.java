package com.carbonit.exercise.model;

import java.util.Objects;

public class Treasure {

    private final Position position;
    private int count;

    public Treasure(int x, int y, int numberTreasure) {
        this.position = new Position(x, y);
        this.count = numberTreasure;
    }

    public Position getPosition() {
        return position;
    }

    public int getCount() {
        return count;
    }

    public boolean isTreasureRemain() {
        return count > 0;
    }

    boolean removePart() {
        if (isTreasureRemain()) {
            this.count--;
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Treasure treasure = (Treasure) o;
        return Objects.equals(getPosition(), treasure.getPosition());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPosition());
    }

    @Override
    public String toString() {
        return String.format("T - %d - %d - %d", position.getX(), position.getY(), count);
    }
}
