package com.carbonit.exercise.model;

import java.util.Optional;
import java.util.stream.Stream;

public enum Orientation {

    NORTH("N") {
        @Override
        public Orientation turn(Movement movement) {
            switch (movement) {
                case D:
                    return Orientation.EAST;
                case G:
                    return Orientation.WEST;
                default:
                    return this;
            }
        }

        @Override
        public Position moveForward(Position currentPosition) {
            Position nextPosition = new Position(currentPosition.getX(), currentPosition.getY());
            nextPosition.setY(nextPosition.getY() - 1);
            return nextPosition;
        }
    },
    SOUTH("S") {
        @Override
        public Orientation turn(Movement movement) {
            switch (movement) {
                case D:
                    return Orientation.WEST;
                case G:
                    return Orientation.EAST;
                default:
                    return this;
            }
        }

        @Override
        public Position moveForward(Position currentPosition) {
            Position nextPosition = new Position(currentPosition.getX(), currentPosition.getY());
            nextPosition.setY(nextPosition.getY() + 1);
            return nextPosition;
        }

    },
    EAST("E") {
        @Override
        public Orientation turn(Movement movement) {
            switch (movement) {
                case D:
                    return Orientation.SOUTH;
                case G:
                    return Orientation.NORTH;
                default:
                    return this;
            }
        }

        @Override
        public Position moveForward(Position currentPosition) {
            Position nextPosition = new Position(currentPosition.getX(), currentPosition.getY());
            nextPosition.setX(nextPosition.getX() + 1);
            return nextPosition;
        }
    },
    WEST("O") {
        @Override
        public Orientation turn(Movement movement) {
            switch (movement) {
                case D:
                    return Orientation.NORTH;
                case G:
                    return Orientation.SOUTH;
                default:
                    return this;
            }
        }

        @Override
        public Position moveForward(Position currentPosition) {
            Position nextPosition = new Position(currentPosition.getX(), currentPosition.getY());
            nextPosition.setX(nextPosition.getX() - 1);
            return nextPosition;
        }
    };

    private final String orientation;

    Orientation(String orientation) {
        this.orientation = orientation;
    }

    public static Optional<Orientation> find(String orientation) {
        return Stream.of(Orientation.values())
                .filter(value -> value.orientation.equals(orientation))
                .findFirst();
    }

    public String getOrientation() {
        return orientation;
    }

    public abstract Orientation turn(Movement movement);

    public abstract Position moveForward(Position currentPosition);
}
