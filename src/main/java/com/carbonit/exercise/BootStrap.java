package com.carbonit.exercise;

import com.carbonit.exercise.model.Map;
import com.carbonit.exercise.service.*;

public class BootStrap {

    public static void main(String[] args) {
        System.out.println(" args length: "+ args.length);
        String filename = args.length > 0 ? args[0] : "map.txt";

        Reader reader = new FileReader();
        Map map = reader.parse(filename);

        TreasureService treasureService = new TreasureService();
        treasureService.findTreasure(map);

        Writer writer = new FileWriter();
        writer.write(map);
    }
}